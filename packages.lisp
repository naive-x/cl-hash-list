(in-package :common-lisp-user)

(defpackage :cl-hash-list
  (:use :cl)
  (:export

   :hash-list
   :hash-list-size
   :hash-list-trees
   :make-hash-list
   :hash-list-p
   :hash-cons
   :hash-lookup
   :hash-map-value
   :hahs-map-key-value
   :hash-list-values
   :hash-list-keys
   :value-search
   :hash-search
   
    
   ))
