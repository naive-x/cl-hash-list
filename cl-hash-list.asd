(defsystem "cl-hash-list"
  :description "This is a naive, persisted, in memory (lazy loading) data store for Common Lisp."
  :version "2020.06.19"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on ()
  :components ((:file "packages")
	       (:file "common" :depends-on ("packages"))
	       (:file "hash-list" :depends-on ("common"))
               (:file "tests" :depends-on ("hash-list"))
	       ))

