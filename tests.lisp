(in-package :cl-hash-list)

(defparameter *sorted-demo-p* t)

(defun demo-hash-list (size)
  (or *db*
      (time
       (let ((hash-list))
	 (dotimes (x size)
	   (setf hash-list (hash-cons (if *sorted-demo-p*
					  x
					  (sxhash (random size))) hash-list)))
	 (setf *db* hash-list)
	 hash-list))))

(defun test-list-lookup (size i)
  (let ((hash-list (demo-hash-list size)))
    (time
     (hash-lookup hash-list i))))


(defun test-hash-map (size)
  (let ((hash-list (demo-hash-list size))
	(*search-node-access-count* 0))
    (time
 
     (hash-map (lambda(node)
	       (declare (ignore node))
	       (incf *search-node-access-count*))
	     hash-list))
    *search-node-access-count*))

(defun test-hash-map-value (size)
  (let ((hash-list (demo-hash-list size))
	(*search-node-access-count* 0))
    (time
 
     (hash-map-value (lambda(value)
		       (declare (ignore value))
		       (incf *search-node-access-count*))
		     hash-list))
    *search-node-access-count*))

(defun test-hash-map-key-value (size)
  (let ((hash-list (demo-hash-list size))
	(*search-node-access-count* 0))
    (time
 
     (hash-map-value (lambda(key value)
		       (declare (ignore value) (ignore key))
		       (incf *search-node-access-count*))
		     hash-list))
    *search-node-access-count*))

(defun test-hash-list-values (size)
  (let ((hash-list (demo-hash-list size)))
    (time
     (hash-list-values hash-list))))

(defun test-hash-list-keys (size)
  (let ((hash-list (demo-hash-list size)))
    (time
     (hash-list-keys hash-list))))


(defun test-value-search (size value)
  (let ((*search-node-access-count* 0)
	(hash-list (demo-hash-list size))
	(result))
    (print"searching")
    (setf *search-node-access-count* 0)
    (time
     (setf result (value-search hash-list value)))
    (print *search-node-access-count*)
    result))

(defun test-hash-search (size value)
  (let ((hash-list (demo-hash-list size))
	(*search-node-access-count* 0)
	(result))
    (print "searching")
    (setf *search-node-access-count* 0)
    
    (time
     (setf result (hash-search hash-list value)))
    (print *search-node-access-count*)
    result))

(defun test-sbcl-hash-table (size lookup-value)
  (let ((table (make-hash-table :test 'equalp)))
    ;;Populating sbcl hash table
    (time
     (dotimes (x size)
       (setf (gethash x table) x)))
    
    (print "Lookup sbcl hash table")
    (time (gethash lookup-value table))
    (hash-table-count table)))


